# Dorji DRA818X Library

This is a library for the [Dorji DRA818X](http://www.dorji.com/docs/data/DRA818V.pdf) radio module.

<a href="http://dorji.com/products-detail.php?ProId=55"><img src="./assets/board.jpg" width="500"/></a>

Source of the picture: [mikrocontroller.net](https://www.mikrocontroller.net/topic/421550#4987750), 21.11.2021.

## Disclaimer

Please be aware of the fact that transmitting with this radio module does require a amateur radio licence. It is legal tough to receive radio signals if you do not own such license.

# Features

The library does provide a simple and secure way (error handling) of interacting with the Dorji DRA818X. All commands which are available for the module are implemented as methods:

- Handshake
- Frequency Scan
- Update group settings
- Volume
- Filter

For the group setting command and the filter command you will first need to set the given parameters with their corresponding setter methods. E.g. `setFrequency()`, `setRxFrequency()`, `setTxCTCSS()`, `setSquelch()`, `setEmphasisFilter()` and so on.

The library also provides error handling for incorrect wirering, wrong parameters, etc.

# Installation

To install the library choose one of the following options

1. Install the library by [using the Library Manager](https://www.arduino.cc/en/Guide/Libraries#toc3)
2. By [importing the .zip library](https://www.arduino.cc/en/Guide/Libraries#toc4) using either the [master](https://codeberg.org/schorsch/Dorji-DRA818X/archive/main.zip) or one of the [releases](https://codeberg.org/schorsch/Dorji-DRA818X/releases) ZIP files.
3. Via PlatformIO by pasting the following code block into your `platformio.ini` file

```
lib_deps =
    https://codeberg.org/schorsch/Dorji-DRA818X.git
```

# Examples

To test not only the functionality of the library but also your radio module, you can use the [ModuleTester.ino](https://codeberg.org/schorsch/Dorji-DRA818X/src/branch/develop/examples/ModuleTester.ino).

# Contributing

If you want to contribute to this library then either create an issue with a feature request / bug report or create a pull requst with your changes.
