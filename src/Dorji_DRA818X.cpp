/**
 *	Dorji DRA818X Library
 *
 *	Authors:
 *		- René Schaar <rene@schaar.priv.at>
 *		- Patrick Zechner <patrick@schwarzerkater.at>
 * 
 *	The Dorji DRA818X does require serial commands to work.
 *	This library provides an interface to communicate with the radio module.
 *   
 *	Please be aware of the fact that transmitting with this radio module does
 *	require a radio amateur licence. It is legal tough to receive radio signals
 *	if you do not own such license.
 *
 *	This library is licensed under the Gnu General Public Licenese version 3.
 *	For further detail, please have a look into the LICENSE file.
 */

#include "../include/Dorji_DRA818X.h"
#include <Arduino.h>
#include <stdio.h>

/**
 * Parameters:
 * 
 * 		uart - Serial connection to the radio module.
 * 		band - 0 -> 2m | 1 -> 70cm
 */
Dorji_DRA818X::Dorji_DRA818X( HardwareSerial *uart, boolean band )
{
	Dorji_DRA818X::_uart = uart;
	Dorji_DRA818X::_band = band;
	Dorji_DRA818X::_channelSpace = 0;
	Dorji_DRA818X::_rxFrequency = band ? DEFAULT_FREQUENCY_70CM : DEFAULT_FREQUENCY_2M;
	Dorji_DRA818X::_txFrequency = band ? DEFAULT_FREQUENCY_70CM : DEFAULT_FREQUENCY_2M;
	Dorji_DRA818X::_rxCTCSS = "0000";
	Dorji_DRA818X::_txCTCSS = "0000";
	Dorji_DRA818X::_volume = 5;
	Dorji_DRA818X::_squelch = 5;
	Dorji_DRA818X::_emphasis = false;
	Dorji_DRA818X::_lowpass = false;
	Dorji_DRA818X::_highpass = false;

	Dorji_DRA818X::txPower = 0;
	Dorji_DRA818X::state = STATE_STAND_BY;

	_uart->begin( 9600 );
}

/**
 * Set the frequency, in MHz, the radio should receive on.
 * 
 * Parameters:
 * 
 * 		double frequency - Optional [145.500/433.000]! Frequency which should be received on.
 * 
 * Returns:
 * 
 * 		0 - Success.
 * 		1 - Frequency out of range.
 */
int Dorji_DRA818X::setRxFrequency( double frequency = -1.0 )
{
	if ( frequency == -1.0 )
	{
		this->_rxFrequency = ( this->_band ? DEFAULT_FREQUENCY_2M : DEFAULT_FREQUENCY_70CM );
		
		return 0;
	}

	if ( this->_band == 0 )
	{
		if ( frequency < MIN_FREQUENCY_2M || frequency > MAX_FREQUENCY_2M )
		{
			return 1;
		}

		this->_rxFrequency = frequency;
	}
	else
	{
		if ( frequency < MIN_FREQUENCY_70CM || frequency > MAX_FREQUENCY_70CM )
		{
			return 1;
		}

		this->_rxFrequency = frequency;
	}

	return 0;
}

/**
 * Set the frequency, in MHz, the radio should send on.
 * 
 * Parameters:
 * 
 * 		doulbe frequency - Optional [145.500/433.000]! Frequency which should be send on.
 * 
 * Returns:
 * 
 * 		0 -> Success.
 * 		1 - Frequency out of range.
 */
int Dorji_DRA818X::setTxFrequency( double frequency = -1.0 )
{
	if ( frequency == -1.0 )
	{
		this->_txFrequency = ( this->_band ? DEFAULT_FREQUENCY_2M : DEFAULT_FREQUENCY_70CM );
		
		return 0;
	}

	if ( this->_band == 0 )
	{
		if ( frequency < MIN_FREQUENCY_2M || frequency > MAX_FREQUENCY_2M )
		{
			return 1;
		}

		this->_txFrequency = frequency;
	}
	else
	{
		if ( frequency < MIN_FREQUENCY_70CM || frequency > MAX_FREQUENCY_70CM )
		{
			return 1;
		}

		this->_txFrequency = frequency;
	}

	return 0;
}

double Dorji_DRA818X::getRxFrequency()
{
	return this->_rxFrequency;
}

double Dorji_DRA818X::getTxFrequency()
{
	return this->_txFrequency;
}

/**
 * Set both the rx and tx frequency at once.
 * 
 * Parameters:
 * 
 * 		double frequency - Optional [145.500/433.000]! Frequency which should be send and received on.
 * 
 * Returns:
 * 
 * 		0 --> Success.
 * 		1 --> Failure. Try setting the rx and tx frequency independently.
 */ 
int Dorji_DRA818X::setFrequency( double frequency = -1 )
{
	return setRxFrequency( frequency ) || setTxFrequency( frequency );
}

/**
 * Increases both the rx and tx frequency by the bandwith
 *
 * Returns:
 * 
 * 		0 --> Success.
 * 		1 --> Failure. Try setting the rx and tx frequency independently.
 */
int Dorji_DRA818X::incFrequency()
{
	double frequency = this->_rxFrequency;
	// channelSpace = 0 -> 12.5; 1 -> 25
	// 0 -> 1 + 0 = 1 -> 1 * 12.5 = 12.5 -> 12.5
	// 1 -> 1 + 1 = 2 -> 2 * 12.5 = 25 -> 25
	frequency += ( 1 + this->_channelSpace ) * 12.5;

	return setRxFrequency( frequency ) || setTxFrequency( frequency );
}

/**
 * Decreases both the rx and tx frequency by the bandwith
 * 
 * Returns:
 * 
 * 		0 --> Success.
 * 		1 --> Failure. Try setting the rx and tx frequency independently.
 */
int Dorji_DRA818X::decFrequency()
{
	double frequency = this->_rxFrequency;
	// channelSpace = 0 -> 12.5; 1 -> 25
	// 0 -> 1 + 0 = 1 -> 1 * 12.5 = 12.5 -> 12.5
	// 1 -> 1 + 1 = 2 -> 2 * 12.5 = 25 -> 25
	frequency -= ( 1 + this->_channelSpace ) * 12.5;

	return setRxFrequency( frequency ) || setTxFrequency( frequency );
}

/**
 * Send a serial command to the radio module to sync the module group settings with those of this class.
 * If the update failes for whatever reason, the onError() method will be called.
 * 
 * Returns:
 * 
 * 		0 --> Success.
 * 		1 --> Failure. Some parameters are out of range or they are missing entirely.
 * 		2 --> Failure. There might be a typo in the command!
 * 		3 --> Failure. Check wirering!
 */
int Dorji_DRA818X::updateGroupSettings()
{
	char *command; 
	asprintf
	( 
		&command, "AT+DMOSETGROUP=%d,%.4f,%.4f,%s,%d,%s\r\n",
		this->_channelSpace, this->_txFrequency, this->_rxFrequency,
		this->_txCTCSS, this->_squelch, this->_rxCTCSS
	);

	int state = serialSend( command, "+DMOSETGROUP:0", "+DMOSETGROUP:1" );

	if ( state > 0 )
	{
		this->onError();
	} 
	
	return state;
}

/**
 * Test the connection between the microcontroller and the radio module.
 * If the test fails the module will restart.
 * 
 * Returns:
 * 
 * 		0 -> Sucess.
 * 		1 -> Failure. The module responded with an error.
 * 		2 --> Failure. There might be a typo in the command!
 * 		3 --> Failure. Check wirering!
 */
int Dorji_DRA818X::handshake()
{
	char *command = "AT+DMOCONNECT\r\n"; 

	int state = serialSend( command, "+DMOCONNECT:0", "+DMOCONNECT:1" );

	if ( state > 0 )
	{
		this->onError();
	} 
	
	return state;
}

/**
 * Scan if there is any signal on the given frequency.
 * 
 * Parameters:
 * 
 * 		double frequency - Frequency to scan if there are any signals.
 * 
 * Returns:
 * 
 * 		0 -> There is a signal.
 * 		1 -> There is no signal.
 * 		2 --> Failure. There might be a typo in the command!
 * 		3 --> Failure. Check wirering!
 */
int Dorji_DRA818X::scanFrequency( double frequency )
{
	char *command; 
	asprintf
	( 
		&command, "S+%3.4f\r\n", frequency
	);

	int state = serialSend( command, "S=0", "S=1" );

	if ( state >= 2 )
	{
		this->onError();
	} 
	
	return state;
}

/**
 * Set the CTCSS code for RX.
 * 
 * Parameters:
 * 
 * 		int ctcss - Optional [0]! CTCSS code [0-38] the radio should use on RX.
 * 
 * Return:
 * 
 * 		0 --> Success.
 * 		1 --> Failure. Code out of range.
 */
int Dorji_DRA818X::setRxCTCSS( int ctcss = -1 )
{
	if ( ctcss == -1 )
	{
		this->_rxCTCSS = "0000";
		
		return 0;
	}

	if ( ctcss < 0 || ctcss > 38 )
	{
		this->_rxCTCSS = "0000";

		return 1;
	}

	this->_rxCTCSS = ( ctcss < 10 ? "000" : "00" ) + String( ctcss );

	return 0;
}

/**
 * Set the CTCSS code for TX.
 * 
 * Parameters:
 * 
 * 		int ctcss - Optional [0]! CTCSS code [0-38] the radio should use on TX.
 * 
 * Return:
 * 
 * 		0 --> Success.
 * 		1 --> Failure. Code out of range.
 */
int Dorji_DRA818X::setTxCTCSS( int ctcss = -1 )
{
	if ( ctcss == -1 )
	{
		this->_txCTCSS = "0000";
		
		return 0;
	}

	if ( ctcss < 0 || ctcss > 38 )
	{
		this->_txCTCSS = "0000";

		return 1;
	}

	this->_txCTCSS = ( ctcss < 10 ? "000" : "00" ) + String( ctcss );

	return 0;
}

/**
 * Send a serial command to the module to update the volume.
 * If the update failes for whatever reason, the onError() method will be called.
 * 
 * Parameters:
 * 
 * 		int volume - Volume range [1-8]
 * 
 * Returns:
 * 
 * 		0 --> Success.
 * 		1 --> Failure. Volume out of range.
 * 		2 --> Failure. There might be a typo in the command.
 * 		3 --> Failure. Check wirering.
 */ 
int Dorji_DRA818X::setVolume( int volume )
{
	if ( volume < 1 || volume > 8 )
	{
		return 1;
	}

	this->_volume = volume;

	char *command; 
	asprintf( &command, "AT+DMOSETVOLUME=%d\r\n", this->_volume );

	int state = serialSend( command, "+DMOSETVOLUME:0", "+DMOSETVOLUME:1" );

	if ( state > 0 )
	{
		this->onError();
	} 
	
	return state;
}

int Dorji_DRA818X::getVolume()
{
	return this->_volume;
}

/**
 * Set the squelch level of the module.
 * 
 * Parameters:
 * 
 * 		int squelch - Optional [3]! - Squelch level [0-8]
 * 
 * Returns:
 * 
 * 		0 --> Success.
 * 		1 --> Failure. Squelch out of range.
 */ 
int Dorji_DRA818X::setSquelch( int squelch = -1 )
{
	if ( squelch == -1 )
	{
		this->_squelch = 3;

		return 0;
	}

	if ( squelch < 0 || squelch > 8 )
	{
		return 1;
	}

	this->_squelch = squelch;

	return 0;
}

int Dorji_DRA818X::getSquelch()
{
	return this->_squelch;
}

/**
 * Turn the emphasis filter of the modules input on/off.
 * 
 * Parameters:
 * 
 * 		int state - 0 --> off | 1 --> on.
 */ 
void Dorji_DRA818X::setEmphasisFilter( int state )
{
	this->_emphasis = state;
}

/**
 * Turn the lowpass filter of the modules output on/off.
 * 
 * Parameters:
 * 
 * 		int state - 0 --> off | 1 --> on.
 */ 
void Dorji_DRA818X::setLowpassFilter( int state )
{
	this->_lowpass = state;
}

/**
 * Turn the highpass filter of the modules output on/off.
 * 
 * Parameters:
 * 
 * 		int state - 0 --> off | 1 --> on.
 */ 
void Dorji_DRA818X::setHighpassFilter( int state )
{
	this->_highpass = state;
}

/**
 * Send a serial command to the module to update the filters.
 * If the update failes for whatever reason, the onError() method will be called.
 * 
 * Returns:
 * 
 * 		0 --> Success.
 * 		1 --> Failure. Volume out of range.
 * 		2 --> Failure. There might be a typo in the command.
 * 		3 --> Failure. Check wirering.
 */ 
int Dorji_DRA818X::updateFilters()
{
	char *command; 
	asprintf( &command, "AT+SETFILTER=%b,%b,%b\r\n", !_emphasis, !_highpass, !_lowpass );

	int state = serialSend( command, "+DMOSETFILTER:0", "+DMOSETFILTER:1" );

	if ( state > 0 )
	{
		this->onError();
	} 
	
	return state;
}

/**
 * Send a serial command to the radio module.
 * 
 * Parameters:
 * 
 * 		char *command - The serial command which should be send according to the format of the datasheet.
 * 		char *success - The modules response string on success.
 * 		char *failure - The modules response string on failure.
 * 
 * Returns:
 * 
 * 		0 --> Success.
 * 		1 --> Failure. Some parameters are out of range or they are missing entirely.
 * 		2 --> Failure. There might be a typo in the command.
 * 		3 --> Failure. Check wirering.
 * 
 * Example:
 * 
 * 		serialSend( "AT+DMOSETVOLUME=5", "+DMOSETVOLUME:0", "+DMOSETVOLUME:1" );
 */ 
int Dorji_DRA818X::serialSend( char *command, char *success, char *failure )
{
	// The module takes about 67ms to response.
	// If the module receives multiple commands in a row,
	// it takes up to twice the time to respond. Therefore we will wait 140ms.
	// If it does not respond after 140ms we try again.
	// After 3 tries we suppose that it failed and the onError() Method will be triggered.
	for ( int i = 0; i < 3; i++ )
	{
		long exceedTime = millis() + 140;
		
		this->_uart->printf( command );
		
		while ( millis() < exceedTime )
		{
			if ( this->_uart->available() )
			{
				// \r\n is the end of the command.
				String response = this->_uart->readStringUntil( '\r' );

				if ( response.equals( success ) )
				{
					return 0;
				}
				else if ( response.equals( failure ) )
				{
					return 1;
				}
				else if ( response.equals( "+DMOERROR" ) )
				{
					return 2;
				}
			}
		}
	}

    return 3;
}
