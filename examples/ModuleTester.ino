/**
*	Dorji DRA818X Library
*
*	Authors:
*		- René Schaar <rene@schaar.priv.at>
*		- Patrick Zechner <patrick@schwarzerkater.at>
*
*	The Dorji DRA818X does require serial commands to work.
*	This library provides an interface to communicate with the radio module.
*   
*	Please be aware of the fact that transmitting with this radio module does
*	require a radio amateur licence. It is legal tough to receive radio signals
*	if you do not own such license.
*
*	This library is licensed under the Gnu General Public Licenese version 3.
*	For further detail, please have a look into the LICENSE file.
*/

/**
 * This example is meant for testing purposes.
 * It uses a 70cm module and sets its frequency to 443.5000 MHz.
 * It does print out all error codes of the needed functions.
 * Use this example to test if the module is working and if the wirering is correct.
 */
#include <Dorji_DRA818X.h>

#define band 1 // 70cm
#define frequency 443.5000

void onError();
Dorji_DRA818X dorji( &Serial2, band );

void setup()
{
	Serial.begin( 9600 );
	
	dorji.onError = &onError;
}

void loop()
{
	Serial.print( "Handshake " ); Serial.println( dorji.handshake() );
	Serial.print( "Scan " ); Serial.println( dorji.scanFrequency( 433.5000 ) );
	Serial.print( "Group " ); Serial.println( dorji.updateGroupSettings() );
	Serial.print( "Filter " ); Serial.println( dorji.updateFilters() );
	Serial.print( "Volume " ); Serial.println( dorji.setVolume( 5 ) );
}

void onError()
{
	Serial.print( "Error " );
}