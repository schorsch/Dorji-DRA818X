/**
*	Dorji DRA818X Library
*
*	Authors:
*		- René Schaar <rene@schaar.priv.at>
*		- Patrick Zechner <patrick@schwarzerkater.at>
*
*	The Dorji DRA818X does require serial commands to work.
*	This library provides an interface to communicate with the radio module.
*   
*	Please be aware of the fact that transmitting with this radio module does
*	require a radio amateur licence. It is legal tough to receive radio signals
*	if you do not own such license.
*
*	This library is licensed under the Gnu General Public Licenese version 3.
*	For further detail, please have a look into the LICENSE file.
*/

#ifndef __DRA818_H__
#define __DRA818_H__

#include <Arduino.h>

#define MIN_FREQUENCY_2M 134.0000
#define MAX_FREQUENCY_2M 174.0000
#define DEFAULT_FREQUENCY_2M 145.5000

#define MIN_FREQUENCY_70CM 400.0000
#define MAX_FREQUENCY_70CM 470.0000
#define DEFAULT_FREQUENCY_70CM 433.0000

#define STATE_STAND_BY 0
#define STATE_RX 1
#define STATE_TX 2

class Dorji_DRA818X
{
	public:
		Dorji_DRA818X( HardwareSerial *uart, boolean band );

		int setRxFrequency( double frequency );
		int setTxFrequency( double frequency );
		double getRxFrequency();
		double getTxFrequency();
		int setFrequency( double frequency );
		int incFrequency();
		int decFrequency();

		int setRxCTCSS( int ctcss );
		int setTxCTCSS( int ctcss );

		int setVolume( int volume );
		int getVolume();

		int setSquelch( int squelch );
		int getSquelch();

		bool txPower;	// 0 --> low; 1 --> high
		int state;		// 0 --> stand-by; 1 --> rx; 2 --> tx;

		void setEmphasisFilter( int state );
		void setLowpassFilter( int state );
		void setHighpassFilter( int state );
		int updateFilters();

		int updateGroupSettings();
		int handshake();
		int scanFrequency( double frequency );

		int serialSend( char *command, char *success, char *failure );

		void ( *onError )();

	private:	
		HardwareSerial *_uart;	// Serial connection
		boolean _band;			// Frequency band. [0] for 2m and [1] for 70cm.
		int _channelSpace;		// Resolution for channel space [0 --> 12.5 / 1 --> 25]kHz. The module works best with 12.5kHz.
		double _rxFrequency;	// Receive frequency. Range: For 2m [134.0000-174.0000]MHz and for 70cm [400.0000-470.0000]MHz.
		double _txFrequency;	// Transmit frequency. Range: For 2m [134.0000-174.0000]MHz and for 70cm [400.0000-470.0000]MHz.
		String _rxCTCSS;		// CTCSS value in receive [0000-0038].
		String _txCTCSS;		// CTCSS value in transmit [0000-0038].
		int _volume;			// Stores the volume of the module in [%].
		int _squelch;			// Squelch level for the module [1-8].
		boolean _emphasis;		// Use a filter for the microphone.
		boolean _lowpass;		// Use a lowpass filter at the output.
		boolean _highpass;		// Use a highpass filter at the output.
};

#endif